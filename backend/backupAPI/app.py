import flask
import flask_restless
from flask_sqlalchemy import SQLAlchemy
import json
from flask import jsonify, request, render_template
from flask_cors import CORS

application = flask.Flask(__name__)
# allow cross domains?
CORS(application)
application.config["DEBUG"] = True
application.config[
    "SQLALCHEMY_DATABASE_URI"
] = "mysql://master:masterpassword@mydb.ceoq3fxzofj3.us-east-1.rds.amazonaws.com/congressexposed"
# application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(application)
db.Model.metadata.reflect(db.engine)


# Class Models


class Bill(db.Model):
    __table__ = db.Model.metadata.tables["Bill"]

    def serialize(self, links=False):
        if links:
            return {
                "bill_id": "{}".format(self.bill_id),
                "sponsoring_member_id": "{}".format(self.sponsoring_member_id),
                "short_title": "{}".format(self.short_title),
                "full_title": "{}".format(self.full_title),
                "summary": "{}".format(self.summary),
                "house_passage_date": date_json(self.house_passage_date),
                "senate_passage_date": date_json(self.senate_passage_date),
                "enacted_date": date_json(self.enacted_date),
                "vetoed_date": date_json(self.vetoed_date),
                "committees": [
                    com.committee_id
                    for com in BillCommittee.query.filter(
                        BillCommittee.bill_id == self.bill_id
                    ).all()
                ],
                "cosponsor": [
                    coop.cosponsor_id
                    for coop in BillCosponsor.query.filter(
                        BillCosponsor.bill_id == self.bill_id
                    ).all()
                ],
                "subcommittee": [
                    subcom.subcommittee_id
                    for subcom in BillSubcommittee.query.filter(
                        BillSubcommittee.bill_id == self.bill_id
                    ).all()
                ],
                "roll_call": [
                    roll_call.serialize()
                    for roll_call in RollCall.query.filter(
                        RollCall.bill_id == self.bill_id
                    ).all()
                ],
                "picture_url": "{}".format(self.picture_url),
            }
        else:
            return {
                "bill_id": "{}".format(self.bill_id),
                "congress": self.congress,
                "enacted_date": date_json(self.enacted_date),
                "short_title": "{}".format(self.short_title),
                "full_title": "{}".format(self.full_title),
                "sponsoring_member_id": "{}".format(self.sponsoring_member_id),
                "house_passage_date": date_json(self.house_passage_date),
                "senate_passage_date": date_json(self.senate_passage_date),
                "summary": "{}".format(self.summary),
                "vetoed_date": date_json(self.vetoed_date),
                "picture_url": "{}".format(self.picture_url),
            }


class BillCommittee(db.Model):
    __table__ = db.Model.metadata.tables["Bill_Committee"]

    def serialize(self):
        return {
            "bill_id": "{}".format(self.bill_id),
            "committee_id": "{}".format(self.committee_id),
        }


class BillCosponsor(db.Model):
    __table__ = db.Model.metadata.tables["Bill_Cosponsor"]

    def serialize(self):
        return {
            "bill_id": "{}".format(self.bill_id),
            "cosponsor_id": "{}".format(self.cosponsor_id),
        }


class BillSubcommittee(db.Model):
    __table__ = db.Model.metadata.tables["Bill_Subcommittee"]

    def serialize(self):
        return {
            "bill_id": "{}".format(self.bill_id),
            "subcommittee_id": "{}".format(self.subcommittee_id),
        }


class Committee(db.Model):
    __table__ = db.Model.metadata.tables["Committee"]

    def serialize(self, links=False):
        if links:
            return {
                "committee_id": "{}".format(self.committee_id),
                "congress": self.congress,
                "name": "{}".format(self.name),
                "website": "{}".format(self.website),
                "chair_party": "{}".format(self.chair_party),
                "subcommittees": [
                    com.serialize()
                    for com in Subcommittee.query.filter(
                        Subcommittee.parent_committee_id == self.committee_id
                    ).all()
                ],
                "member_role_committee": [
                    com.serialize()
                    for com in MemberRoleCommittee.query.filter(
                        MemberRoleCommittee.committee_id == self.committee_id
                    ).all()
                ],
                # "picture_url": "{}".format(self.picture_url),
            }
        else:
            return {
                "committee_id": "{}".format(self.committee_id),
                "congress": self.congress,
                "name": "{}".format(self.name),
                "website": "{}".format(self.website),
                "chair_party": "{}".format(self.chair_party),
                # "picture_url": "{}".format(self.picture_url),
            }


class Issue(db.Model):
    __table__ = db.Model.metadata.tables["Issue"]

    def serialize(self, links=False):
        bills = [
            ib.bill_id
            for ib in IssueBill.query.filter(IssueBill.issue_id == self.issue_id)
        ]
        if links:
            return {
                "issue_id": self.issue_id,
                "name": "{}".format(self.name),
                "picture_url": "{}".format(self.picture_url),
                "number_of_bills": len(bills),
                "bills": bills,
                "lobbying": "{}".format(self.lobbying),
            }
        else:
            return {
                "issue_id": self.issue_id,
                "name": "{}".format(self.name),
                "picture_url": "{}".format(self.picture_url),
                "number_of_bills": len(bills),
                "lobbying": "{}".format(self.lobbying),
            }


class IssueBill(db.Model):
    __table__ = db.Model.metadata.tables["Issue_Bill"]

    def serialize(self):
        return {"issue_id": self.issue_id, "bill_id": "{}".format(self.bill_id)}


class Member(db.Model):
    __table__ = db.Model.metadata.tables["Member"]

    def serialize(self, links=False):

        if links:
            return {
                "member_id": "{}".format(self.member_id),
                "first_name": "{}".format(self.first_name),
                "middle_name": "{}".format(self.middle_name),
                "last_name": "{}".format(self.last_name),
                "full_name": full_name(
                    self.first_name, self.middle_name, self.last_name
                ),
                "twitter_account": "{}".format(self.twitter_account),
                "facebook_account": "{}".format(self.facebook_account),
                "party": "{}".format(self.party),
                "biography": "{}".format(self.biography),
                "picture_url": "{}".format(self.picture_url),
                "member_role": [
                    member_role.serialize()
                    for member_role in MemberRole.query.filter(
                        MemberRole.member_id == self.member_id
                    ).all()
                ],
                "recent_votes": get_recent_votes(self.member_id),
            }
        else:
            return {
                "date_of_birth": "{}".format(self.date_of_birth),
                "member_id": "{}".format(self.member_id),
                "full_name": full_name(
                    self.first_name, self.middle_name, self.last_name
                ),
                "twitter_account": "{}".format(self.twitter_account),
                "facebook_account": "{}".format(self.facebook_account),
                "party": "{}".format(self.party),
                "biography": "{}".format(self.biography),
                "picture_url": "{}".format(self.picture_url),
                "website": "{}".format(self.website),
            }


class MemberRole(db.Model):
    __table__ = db.Model.metadata.tables["Member_Role"]

    def serialize(self):
        return {
            "member_role_id": self.member_role_id,
            "member_id": "{}".format(self.member_id),
            "member": [
                member.serialize()
                for member in Member.query.filter(
                    Member.member_id == self.member_id
                ).all()
            ],
            "congress": self.congress,
            "chamber": "{}".format(self.chamber),
            "title": "{}".format(self.title),
            "short_title": "{}".format(self.short_title),
            "party": "{}".format(self.party),
            "leadership_role": self.leadership_role,
            "district": self.district,
            "state": "{}".format(self.state),
            "missed_votes_percent": self.missed_votes_percent,
            "votes_with_party_percent": self.votes_with_party_percent,
            "start_date": "{}".format(self.start_date),
            "end_date": "{}".format(self.end_date),
        }


class MemberRoleCommittee(db.Model):
    __table__ = db.Model.metadata.tables["Member_Role_Committee"]

    def serialize(self):
        return {
            "member_role_committee_id": self.member_role_committee_id,
            "member_role_id": self.member_role_id,
            "member_role": [
                member_role.serialize()
                for member_role in MemberRole.query.filter(
                    MemberRole.member_role_id == self.member_role_id
                ).all()
            ],
            "congress": self.congress,
            "committee_id": "{}".format(self.committee_id),
            "title": "{}".format(self.title),
            "rank_in_party": self.rank_in_party,
        }


class MemberRoleSubcommittee(db.Model):
    __table__ = db.Model.metadata.tables["Member_Role_Subcommittee"]

    def serialize(self):
        return {
            "member_role_subcommittee_id": self.member_role_subcommittee_id,
            "member_role_id": self.member_role_id,
            "congress": self.congress,
            "subcommittee_id": "{}".format(self.subcommittee_id),
            "title": "{}".format(self.title),
            "rank_in_party": self.rank_in_party,
        }


class RollCall(db.Model):
    __table__ = db.Model.metadata.tables["RollCall"]

    def serialize(self):
        return {
            "congress": self.congress,
            "rollcall_id": self.rollcall_id,
            "bill_id": "{}".format(self.bill_id),
            "roll_call_date": "{}".format(self.roll_call_date),
            "session_number": self.session_number,
            "chamber": "{}".format(self.chamber),
            "roll_call_number": self.roll_call_number,
        }


class RollCallPosition(db.Model):
    __table__ = db.Model.metadata.tables["RollCallPosition"]

    def serialize(self, links=False):
        return {
            "rollcall_id": self.rollcall_id,
            "member_id": "{}".format(self.member_id),
            "vote_position": "{}".format(self.vote_position),
            "dw_nominate": self.dw_nominate,
        }


class Subcommittee(db.Model):
    __table__ = db.Model.metadata.tables["Subcommittee"]

    def serialize(self, links=False):
        if links:
            return {
                "subcommittee_id": "{}".format(self.subcommittee_id),
                "congress": self.congress,
                "parent_committee_id": "{}".format(self.parent_committee_id),
                "name": "{}".format(self.name),
                "website": self.website,
                "chair_party": "{}".format(self.chair_party),
                "member_role_subcommittee": [
                    com.serialize()
                    for com in MemberRoleSubcommittee.query.filter(
                        MemberRoleSubcommittee.subcommittee_id == self.subcommittee_id
                    ).all()
                ],
            }
        else:
            return {
                "subcommittee_id": "{}".format(self.subcommittee_id),
                "congress": self.congress,
                "parent_committee_id": "{}".format(self.parent_committee_id),
                "name": "{}".format(self.name),
                "website": self.website,
                "chair_party": "{}".format(self.chair_party),
            }


# Helper functions


def date_json(date):
    if date is None:
        return None
    return "{}".format(date)


def full_name(first_name, middle_name, last_name):
    if middle_name is None or middle_name == "":
        return "%s %s" % (first_name, last_name)
    return "%s %s %s" % (first_name, middle_name, last_name)


def get_recent_votes(member_id):
    ids = [
        rc_id.rollcall_id
        for rc_id in RollCallPosition.query.filter(
            RollCallPosition.member_id == member_id
        )
    ]
    ret = []
    for i in ids:
        if len(ret) == 10:
            break
        ret += [RollCall.query.filter_by(rollcall_id=i).first().serialize()]
    return ret


def get_page(page):
    ret = dict()
    ret["num_results"] = page.total
    bill = page.items
    obj = []
    for i in bill:
        obj.append(i.serialize())

    ret["objects"] = obj
    ret["page"] = page.page
    ret["total_pages"] = page.pages
    return ret


def pagination():
    per_page = request.args.get("result_per_page")
    page_num = request.args.get("page")
    if per_page is None:
        per_page = 9
    if page_num is None:
        page_num = 1
    return per_page, page_num


# API endpoints


@application.route("/")
@application.route("/api")
def congress_expose():
    intro = list()
    intro.append(
        """In order to view the api page, google chrome extension: jsonview is highly recommended:
                https://chrome.google.com/webstore/detail/jsonview/chklaanhfefbnpoihckbnefhakgolnmc?hl=en"""
    )
    intro.append("""Here is a list of urls for four main model:""")
    intro.append("""    List of Bills: api.congressexposed.me/api/bill""")
    intro.append("""    Specific Bill: api.congressexposed.me/api/bill/{bill-id}""")
    intro.append("""    List of Issues: api.congressexposed.me/api/issue""")
    intro.append("""    Specific Issue: api.congressexposed.me/api/issue/{issue-id}""")
    intro.append("""    List of Committees: api.congressexposed.me/api/committee""")
    intro.append(
        """    Specific Committee: api.congressexposed.me/api/committee/{committee-id}"""
    )
    intro.append("""    List of Congress People: api.congressexposed.me/api/member""")
    intro.append(
        """    Specific Congress People: api.congressexposed.me/api/member/{member-id}"""
    )
    intro.append("""When requesting a list of objects, pagination is present""")
    intro.append(
        """The default result per page is 9, to get more or less result_per_page:
    Ex: api.congressexposed.me/api/issue?result_per_page=3"""
    )
    intro.append(
        """The default page is the first page, to get specific page:
        Ex: api.congressexposed.me/api/issue?page=3"""
    )
    return render_template("view.html", intro=intro)


# @application.route("/api/test/<member_id>")
# def get_test(member_id):
#     member = [i.member_role_id for i in MemberRole.query.filter_by(member_id=member_id).all()]
#     rc = [MemberRoleCommittee.query.filter_by(member_role_id=j).first().committee_id for j in member]
#     com = [Committee.query.filter_by(committee_id=k).first() for k in rc]
#     s = [w.serialize() for w in com]
#     return jsonify(s)
#
#
# @application.route("/api/test/<committee_id>")
# def get_test2(committee_id):
#     committee = [i.member_role_id for i in MemberRoleCommittee.query.filter_by(committee_id=committee_id).all()]
#     rc = [MemberRole.query.filter_by(member_role_id=j).first().member_id for j in committee]
#     com = [Member.query.filter_by(member_id=k).first() for k in rc]
#     s = [w.serialize() for w in com]
#     return jsonify(committee)


@application.route("/api/bill")
def get_bill():
    per_page, page_num = pagination()
    page = Bill.query.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/issue")
def get_issue():
    per_page, page_num = pagination()
    page = Issue.query.order_by(Issue.issue_id).paginate(
        per_page=int(per_page), page=int(page_num)
    )
    return jsonify(get_page(page))


@application.route("/api/member")
def get_member():
    per_page, page_num = pagination()
    page = Member.query.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/committee")
def get_committee():
    per_page, page_num = pagination()
    page = Committee.query.filter_by(congress=116).paginate(
        per_page=int(per_page), page=int(page_num)
    )
    return jsonify(get_page(page))


@application.route("/api/subcommittee")
def get_subcommittee():
    per_page, page_num = pagination()
    page = Subcommittee.query.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/bill_committee")
def get_bill_committee():
    per_page, page_num = pagination()
    page = BillCommittee.query.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/bill_cosponsor")
def get_bill_cosponsor():
    per_page, page_num = pagination()
    page = BillCosponsor.query.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/bill_subcommittee")
def get_bill_subcommittee():
    per_page, page_num = pagination()
    page = BillSubcommittee.query.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/issue_bill")
def get_issue_bill():
    per_page, page_num = pagination()
    page = IssueBill.query.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/roll_call_position")
def get_roll_call_position():
    per_page, page_num = pagination()
    page = RollCallPosition.query.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/roll_call")
def get_roll_call():
    per_page, page_num = pagination()
    page = RollCall.query.order_by(RollCall.roll_call_date.desc()).paginate(
        per_page=int(per_page), page=int(page_num)
    )
    return jsonify(get_page(page))


@application.route("/api/member_role")
def get_member_role():
    per_page, page_num = pagination()
    page = MemberRole.query.paginate(per_page=int(per_page), page=int(page_num))
    return jsonify(get_page(page))


@application.route("/api/member_role_committee")
def get_member_role_committee():
    per_page, page_num = pagination()
    page = MemberRoleCommittee.query.paginate(
        per_page=int(per_page), page=int(page_num)
    )
    return jsonify(get_page(page))


@application.route("/api/member_role_subcommittee")
def get_member_role_subcommittee():
    per_page, page_num = pagination()
    page = MemberRoleSubcommittee.query.paginate(
        per_page=int(per_page), page=int(page_num)
    )
    return jsonify(get_page(page))


# Specific object
@application.route("/api/issue/<issue_id>")
def get_specific_issue(issue_id):
    issue = Issue.query.filter_by(issue_id=issue_id).first_or_404()
    return json.dumps(issue.serialize(links=True), ensure_ascii=False, indent=None)


@application.route("/api/bill/<bill_id>")
def get_specific_bill(bill_id):
    bill = Bill.query.filter_by(bill_id=bill_id).first_or_404()
    return json.dumps(bill.serialize(links=True), ensure_ascii=False, indent=None)


@application.route("/api/member/<member_id>")
def get_specific_member(member_id):
    member = Member.query.filter_by(member_id=member_id).first_or_404()
    return json.dumps(member.serialize(links=True), ensure_ascii=False, indent=None)


@application.route("/api/committee/<committee_id>")
def get_specific_committees(committee_id):
    com = [
        com_cong.serialize(links=True)
        for com_cong in Committee.query.filter(
            Committee.committee_id == committee_id
        ).all()
    ]
    return json.dumps(com, ensure_ascii=False, indent=None)


@application.route("/api/subcommittee/<subcommittee_id>")
def get_specific_subcommittees(subcommittee_id):
    subcom = [
        com_cong.serialize(links=True)
        for com_cong in Subcommittee.query.filter(
            Subcommittee.subcommittee_id == subcommittee_id
        ).all()
    ]
    return json.dumps(subcom, ensure_ascii=False, indent=None)


@application.route("/api/member_role/<member_role_id>")
def get_specific_member_role(member_role_id):
    mem = [
        memr.serialize(links=True)
        for memr in MemberRole.query.filter(
            MemberRole.subcommittee_id == member_role_id
        ).all()
    ]
    return json.dumps(mem, ensure_ascii=False, indent=None)


@application.route("/api/member_role_committee/<member_role_committee_id>")
def get_specific_member_role_committee(member_role_committee_id):
    mem = [
        mrc.serialize(links=True)
        for mrc in Subcommittee.query.filter(
            Subcommittee.subcommittee_id == member_role_committee_id
        ).all()
    ]
    return json.dumps(mem, ensure_ascii=False, indent=None)


@application.route("/api/member_role_subcommittee/<member_role_subcommittee_id>")
def get_specific_member_role_subcommittee(member_role_subcommittee_id):
    mem = [
        mrs.serialize(links=True)
        for mrs in Subcommittee.query.filter(
            Subcommittee.subcommittee_id == member_role_subcommittee_id
        ).all()
    ]
    return json.dumps(mem, ensure_ascii=False, indent=None)


if __name__ == "__main__":
    application.run()
