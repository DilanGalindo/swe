import React from 'react';

export default function PartyLabel (props) {
  const firstChar = props.party.charAt(0).toLowerCase();
  let color = "gray";
  if (firstChar === 'r') {
    color = "red";
  } else if (firstChar === 'd') {
    color = "blue";
  } else if (firstChar === 'l') {
    color = "yellow";
  }
  return (<font color={color}>{props.party}</font>);
}
