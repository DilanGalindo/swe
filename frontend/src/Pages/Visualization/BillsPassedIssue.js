import React, { Component } from 'react';
import makeCancelable from '../../Misc/makeCancelable.js';
import { apiFetch } from '../../Misc/API.js';
import Trump from "../../Components/Trump.gif";
import {PieChart} from 'react-charts-d3';

class BillsPassedIssue extends Component {

    constructor (props) {
        super(props);
        this.state = { data: null };
    }

    componentWillMount() {
        this.apiPromise_ = makeCancelable(apiFetch("/api/visualize_2").then((data) => this.setState({data: data})));
    }

    render () {
        if (this.state.data === null) {
            return (
                <div>
                  <div style={{textAlign: "center"}}>
                    <img src={Trump} height="35%" width="35%" alt="rocket gif" style={{ marginTop: "1.5%"}} />
                    <p>
                      Loading...
                    </p>
                  </div>
                </div>);
        } else {
              const visual = this.state.data;
              let dataset = [];
              var n = 0;
              for (var i in visual) {
                if(visual[i] < 2) {
                  n++;
                } else {
                  dataset.push({
                      label: i,
                      value: visual[i]
                  });
                }
              }
              dataset.push({
                  label: 'other',
                  value: 2
              });
            return (
                <div>
                    <PieChart
                      width={1200}
                      height={1200}
                      labelOffset={400}
                      data={dataset}
                      labels />
               </div>
            );
        }
    }

}

export default BillsPassedIssue;
