import React, { Component } from 'react';
import makeCancelable from '../../../Misc/makeCancelable.js';
import { customerAPIFetchAll } from './API.js';
import Trump from "../../../Components/Trump.gif";
import { PieChart } from 'react-charts-d3';

class HabitatPieChart extends Component {

    constructor (props) {
        super(props);
        this.state = { animalData: null, plantData: null };
    }

    componentWillMount() {
        this.apiPromise_ = makeCancelable(Promise.all([customerAPIFetchAll("animals"), customerAPIFetchAll("plants")])
                                          .then(([animalData, plantData]) => this.setState({animalData: animalData, plantData: plantData})));
    }

    render () {
        if (this.state.animalData === null && this.state.plantData === null) {
            return (
                <div>
                  <div style={{textAlign: "center"}}>
                    <img src={Trump} height="35%" width="35%" alt="rocket gif" style={{ marginTop: "1.5%"}} />
                    <p>
                      Loading...
                    </p>
                  </div>
                </div>);
        } else {

            let habitats = {};
            let total = 0;
            for (let i = 0; i < this.state.animalData.length; i++) {
                let animal = this.state.animalData[i];
                if (animal.category === "Secure") continue;
                for (let j = 0; j < animal.habitats.length; j++) {
                    let habitat = animal.habitats[j].habitat_name;
                    if (habitats[habitat] === undefined) {
                        habitats[habitat] = 1;
                    } else {
                        habitats[habitat] += 1;
                    }
                    total += 1;
                }
            }

            let threshold = 2;
            let dataset = [{ label: "Other", value: 0}];
            for (let habitat in habitats) {
                let percentage = habitats[habitat] / total * 100.0;
                if (percentage < threshold) {
                    dataset[0].value += percentage;
                } else {
                    dataset.push({ label: habitat, value: percentage });
                }

            }

            console.log(dataset);

            return (
                <div>
                  <PieChart
                    width={1200}
                    height={1200}
                    labelOffset={400}
                    data={dataset}
                    labels />
               </div>
            );
        }
    }

}

export default HabitatPieChart;
