import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import styled from 'styled-components/macro';
import { Card, CardBody, CardTitle, Row, Col, Container } from 'reactstrap';
import RenderCards from '../../Components/RenderCards.js';
import PartyLabel from '../../Components/PartyLabel.js';
import ModelPagination from '../../Components/ModelPagination.js';
import TwitterEmbed from '../../Components/TwitterEmbed.js';
import FacebookEmbed from '../../Components/FacebookEmbed.js';
import TextHighlight from '../../Components/TextHighlight.js';
import ModelManyRelationshipPagination from '../../Components/ModelManyRelationshipPagination.js';
import { BillModelPageContent } from '../Bills/Bills.js';
import { IssueModelPageContent } from '../Issue/Issue.js';
import { absolutePath } from '../../Misc/API.js';
import makeCancelable from '../../Misc/makeCancelable.js';
import { Member, Bill, Issue } from '../../Misc/Models.js';
import '../../Components/Card.css';
import '../../Components/IndividualPages.css';

function makeFullName(firstName, middleName, lastName) {
  return firstName + (middleName === null ? " " : " " + middleName + " ") + lastName;
}


const CongresspeopleCard = function (props) {
  const CardImg = styled.img`
    max-width: 100%;
    max-height: 58%;
    min-height: 58%;
    width: auto;
    height: auto;
    object-fit: cover;
  `;

  const twitter_li = props.data.getAttribute("twitter_account") !== null ?
        (<li>Twitter: <TextHighlight filterText={[props.filter["twitter_account"]]} text={props.data.getAttribute("twitter_account")} /> </li>) : null;
  const facebook_li = props.data.getAttribute("facebook_account") !== null ?
        (<li>Facebook: <TextHighlight filterText={[props.filter["facebook_account"]]} text={props.data.getAttribute("facebook_account")} /> </li>) : null;
  const image = props.data.getAttribute("picture_url") !== null ?
        (<CardImg alt={props.data.full_name} src={absolutePath(props.data.getAttribute("picture_url"))} />) : null;
  const party = props.data.getAttribute("party") !== null ? (<li>Party: <PartyLabel party={props.data.getAttribute("party")} /></li>) : null;

  const full_name = makeFullName(props.data.getAttribute("first_name"), props.data.getAttribute("middle_name"), props.data.getAttribute("last_name"));
  console.log(props.data.getRelationship("roles"));
  return (
    <Card className="card h-100 w-100">
      {image}
      <CardBody className="cardbody">
        <CardTitle>
          <h3>
            <Link
              to={{
                pathname: `/Congresspeople/${props.data.getAttribute("member_id")}`,
                state: { filter: props.filter }
              }}>
              <TextHighlight filterText={[props.filter["first_name"], props.filter["middle_name"], props.filter["last_name"]]}
                             text={full_name} />
            </Link>
          </h3>
        </CardTitle>
        <ul>
          {party}
          {twitter_li}
          {facebook_li}
        </ul>
      </CardBody>
    </Card>
  );
};

class CongresspeopleInstancePage extends Component {
  constructor (props) {
    super(props);
    this.state = {data: null};
    this.apiPromise_ = null;
  }

  componentWillMount () {
    this.apiPromise_ = makeCancelable(Member.fromID(this.props.match.params.congresspeopleID).then((data) => this.setState({data: data})));
  }

  componentWillUnmount () {
    if (this.apiPromise_ !== null) {
      this.apiPromise_.cancel();
      this.apiPromise_ = null;
    }
  }

  filter() {
    if (this.props.location.state === undefined || this.props.location.state.filter === undefined) {
      return {};
    } else {
      return this.props.location.state.filter;
    }
  }

  render () {
    const formatBiography = function (biographyText, filterText) {
      const paragraphs = biographyText.split("\n")
            .filter(el => el.length > 0)
            .map((paragraph, index) => (
              <p key={index}>
                <TextHighlight filterText={filterText} text={paragraph} />
              </p>
            ));
      return paragraphs;
    };

    if (this.state.data === null) {
      return (<div>Loading</div>);
    } else {

      const full_name = makeFullName(this.state.data.getAttribute("first_name"), this.state.data.getAttribute("middle_name"), this.state.data.getAttribute("last_name"));

      const twitter_li = this.state.data.getAttribute("twitter_account") !== null ? (<TwitterEmbed profile={this.state.data.getAttribute("twitter_account")} />) : null;
      const facebook_li = this.state.data.getAttribute("facebook_account") !== null ? (<FacebookEmbed profile={this.state.data.getAttribute("facebook_account")} />) : null;
      const image = this.state.data.getAttribute("picture_url") !== null ? (<img alt={full_name} src={absolutePath(this.state.data.getAttribute("picture_url"))} />) : null;
      const party = this.state.data.getAttribute("party") !== null ? (<PartyLabel party={this.state.data.getAttribute("party")} />) : null;
      const biography = (<p><Row><section id="indivHead"><h5>Biography:</h5></section></Row><Row>{
        formatBiography(this.state.data.getAttribute("biography"), [this.filter()["biography"]])
      }</Row></p>);

      const sponsoredBills = (<ModelManyRelationshipPagination promiseArray={this.state.data.getRelationship("sponsored_bills").asArray()} model={Bill} pageContentComponent={BillModelPageContent} />);

      const relatedIssues = (<ModelManyRelationshipPagination
                             promiseArray={this.state.data.getRelationship("related_issues").asArray()}
                             model={Issue}
                             pageContentComponent={IssueModelPageContent} />);


      return (
        <Container>
          <Row>
            <Col sm={2}></Col>
            <Col sm={8}><section id="firstimage">{image}</section></Col>
            <Col sm={2}></Col>
          </Row>
          <Row>
            <Col sm={4}></Col>
            <Col sm={4} className="text-center"><h1>{full_name}</h1></Col>
            <Col sm={4}></Col>
          </Row>
          <p></p>
          <Row>
          <section id="indivHead"><h5>Social Media:</h5></section>
          </Row>
          <p></p>
          <Row>
            <Col sm={6}><section id="socialmedia">{twitter_li}</section></Col>
            <Col sm={6}><section id="socialmedia">{facebook_li}</section></Col>
          </Row>
          <p></p>
          <Row>
            <section id="indivHead"><h5>Party: {party}</h5></section>
          </Row>
          <p></p>
          {biography}
          <Row>
            <section id="indivHead"><h5>Sponsored Bills:</h5></section>
          </Row>
          <Row>
            {sponsoredBills}
          </Row>
          <Row>
            <section id="indivHead"><h5>Related issues:</h5></section>
          </Row>
          <Row>
            {relatedIssues}
          </Row>
        </Container>);
    }
  }
}

class CongresspeopleModelPageContent extends Component {

  render () {
    const cards = this.props.data.map(t => (<CongresspeopleCard data={t} filter={this.props.filter} location={this.props.location} />));
    return (<RenderCards maxPerRow={3} cards={cards} />);
  }
}


class CongresspeopleModelPage extends Component {

  render() {
    const renderData = function (data) {
      return (<CongresspeopleModelPageContent data={data} filter={this.props.filter} />);
    }.bind(this);

    return (<ModelPagination
            model={Member}
            render={renderData}
            filter={this.props.filter}
            onFilterChange={this.props.onFilterChange}
            showFilter={this.props.showFilter}
            />);
  }
}

class CongresspeopleModelPageController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: {}
    };
  }

  render() {
    return (<CongresspeopleModelPage filter={this.state.filter} onFilterChange={(newFilter) => this.setState({filter: newFilter})} showFilter={true} />);
  }
}

export { makeFullName, CongresspeopleModelPage, CongresspeopleModelPageContent };

export default class Congresspeoples extends Component {
  render() {
    return (
      <div>
        <Route exact path={this.props.match.path} component={CongresspeopleModelPageController} />
        <Route exact path={`${this.props.match.path}/:congresspeopleID`} component={CongresspeopleInstancePage} />
      </div>
    );
  }
}
