import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import { Card, CardBody, CardTitle, Row, Col, Container } from 'reactstrap';
import RenderCards from '../../Components/RenderCards.js';
import ModelPagination from '../../Components/ModelPagination.js';
import ModelManyRelationshipPagination from '../../Components/ModelManyRelationshipPagination.js';
import TextHighlight from '../../Components/TextHighlight.js';
import makeCancelable from '../../Misc/makeCancelable.js';
import { Committee, Member, Bill } from '../../Misc/Models.js';
import { CongresspeopleModelPageContent } from '../Congresspeople/Congresspeople.js';
import { BillModelPageContent } from '../Bills/Bills.js';
import '../../Components/Card.css';
import '../../Components/IndividualPages.css';
import PartyLabel from '../../Components/PartyLabel.js';

const CommitteeCard = function (props) {
  const chair_party = props.data.getAttribute("chair_party") !== null ? (<li>Chair Party: {props.data.getAttribute("chair_party")} </li>) : null;
  const committee_code = props.data.getAttribute("committee_code") !== null ?
        (<li>Committee ID: <TextHighlight filterText={[props.filter["committee_code"]]} text={props.data.getAttribute("committee_code")} /> </li>) : null;
  const congress = props.data.getAttribute("congress") !== null ? (<li>Congress: {props.data.getAttribute("congress")} </li>) : null;
  const website = props.data.getAttribute("website") !== null ?
        (<li>Website: <a href={props.data.getAttribute("website")}><TextHighlight filterText={[props.filter["website"]]} text={props.data.getAttribute("website")} /></a> </li>) : null;
  return (
    <Card className="card h-100 w-100">
      <CardBody className="cardbody">
      <CardTitle>
        <h3>
          <Link to={{
                  pathname: `/Committees/${props.data.getAttribute("committee_id")}`,
                  state: { filter: props.filter }
                }}>
            <TextHighlight filterText={[props.filter["name"]]} text={props.data.getAttribute("name")} />
          </Link>
        </h3>
      </CardTitle>
        <ul>
          {chair_party}
          {committee_code}
          {congress}
          {website}
        </ul>
      </CardBody>
    </Card>
  );
};

class CommitteeInstancePage extends Component {
  constructor (props) {
    super(props);
    this.state = {data: null};
    this.apiPromise_ = null;
  }
  componentWillMount () {
    this.apiPromise_ = makeCancelable(Committee.fromID(this.props.match.params.committeeID).then((data) => this.setState({data: data})));
  }
  componentWillUnmount () {
    if (this.apiPromise_ !== null) {
      this.apiPromise_.cancel();
      this.apiPromise_ = null;
    }
  }

  filter() {
    if (this.props.location.state === undefined || this.props.location.state.filter === undefined) {
      return {};
    } else {
      return this.props.location.state.filter;
    }
  }

  render () {
    if(this.state.data === null) {
      return (<div>Loading</div>);
    } else {
      let filter = this.filter();
      const chair_party = this.state.data.getAttribute("chair_party") !== null ?
            (<PartyLabel party={this.state.data.getAttribute("chair_party")} />) : null;
      const congress = this.state.data.getAttribute("congress") !== null ?
            (<li> {this.state.data.getAttribute("congress")} </li>) : null;
      const name = this.state.data.getAttribute("name") !== null ?
            (<li> <TextHighlight filterText={[filter["name"]]} text={this.state.data.getAttribute("name")} /> </li>) : null;
      const website = this.state.data.getAttribute("website") !== null ?
            (<li>
             <a href={this.state.data.getAttribute("website")}><TextHighlight filterText={[filter["website"]]} text={this.state.data.getAttribute("website")} /></a>
             </li>) : null;
      const relatedMembers = (<ModelManyRelationshipPagination
                              promiseArray={this.state.data.getRelationship("members").asArray()}
                              model={Member}
                              pageContentComponent={CongresspeopleModelPageContent} />);
      const relatedBills = (<ModelManyRelationshipPagination
                              promiseArray={this.state.data.getRelationship("bills").asArray()}
                              model={Bill}
                              pageContentComponent={BillModelPageContent} />);
      return (
        <Container>
           <br></br>
           <Row>
            <Col sm={4}></Col>
            <Col sm={4} className="text-center"><h1><TextHighlight filterText={[filter["committee_code"]]}
              text={this.state.data.getAttribute("committee_code")} /></h1></Col>
            <Col sm={4}></Col>
          </Row>
          <Row>
            <section id="indivHead"><h5>Chair Party: {chair_party}</h5></section>
          </Row>
          <p></p>
          <Row>
            <section id="indivHead"><h5>Congress, Name, and Website: </h5></section>
          </Row>
          <Row>
            <ul>
              {congress}
              {name}
              {website}
            </ul>
          </Row>
          <Row>
            <section id="indivHead"><h5>Committee members: </h5></section>
          </Row>
          <Row>
            {relatedMembers}
          </Row>
          <Row>
            <section id="indivHead"><h5>Sponsored bills: </h5></section>
          </Row>
          <Row>
            {relatedBills}
          </Row>
        </Container>);
    }
  }
}

class CommitteeModelPageContent extends Component {
  render () {
    const cards = this.props.data.map(t => (<CommitteeCard data={t} filter={this.props.filter} />));
    return (<RenderCards maxPerRow={3} cards={cards} />);
  }
}

class CommitteeModelPage extends Component {


  render () {
    const renderData = function (data) {
      return (<CommitteeModelPageContent data={data} filter={this.props.filter} />);
    }.bind(this);

    return (<ModelPagination
            model={Committee}
            render={renderData}
            filter={this.props.filter}
            onFilterChange={this.props.onFilterChange}
            showFilter={this.props.showFilter}
            />);
  }
}

class CommitteeModelPageController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: {}
    };
  }

  render() {
    return (<CommitteeModelPage filter={this.state.filter} onFilterChange={(newFilter) => this.setState({filter: newFilter})} showFilter={true} />);
  }
}

export { CommitteeModelPage, CommitteeModelPageContent };

export default class Committees extends Component {
  render() {
    return (
      <div>
        <Route exact path={this.props.match.path} component={CommitteeModelPageController} />
        <Route exact path={`${this.props.match.path}/:committeeID`} component={CommitteeInstancePage} />
      </div>
    );
  }
}
