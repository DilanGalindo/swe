import assert from 'assert';

const fetchJSON = function (response) {
  return response.json();
};

export default class Api {
  constructor(endpointURL) {
    this.endpointURL = endpointURL;
  }

  bills() {
    return fetch(this.endpointURL + "/v1/bills").then(fetchJSON);
  }

  bill(id) {
    assert(typeof id === "string");
    return fetch(this.endpointURL + "/v1/bills/" + id).then(fetchJSON);
  }

  committees() {
    return fetch(this.endpointURL + "/v1/committees").then(fetchJSON);
  }

  committee(id) {
    assert(typeof id === "string");
    return fetch(this.endpointURL + "/v1/committees/" + id).then(fetchJSON);
  }

  congressMembers() {
    return fetch(this.endpointURL + "/v1/congress-member").then(fetchJSON);
  }

  congressMember(id) {
    assert(typeof id === "string");
    return fetch(this.endpointURL + "/v1/congress-member/" + id).then(fetchJSON);
  }
}
