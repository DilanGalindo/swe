import requests
import json
import os
import sys
import zipfile
import io
from collections import defaultdict
from bs4 import BeautifulSoup

PROPUBLICA_API_URL_BASE = "https://api.propublica.org"
PROPUBLICA_API_HEADERS = {"X-API-Key": "1eEGtCWhmiCYCssFU1dQBKjzkDAH6vwMHMT241WD"}
MIN_CONGRESS_NUM = 115
MAX_CONGRESS_NUM = 116

import atexit

registered_save = {}


def exit_handler():
    for (filename, (dirty, cache)) in registered_save.items():
        if not dirty:
            continue
        try:
            with open(filename, "w+") as f:
                x = True
                while x:
                    try:
                        print("[SAVE]", filename, file=sys.stderr)
                        json.dump(cache, f)
                        x = False
                    except KeyboardInterrupt:
                        x = True
        except KeyboardInterrupt:
            pass


atexit.register(exit_handler)


def file_backed_cache(filename):
    def decorator(fn):
        try:
            with open(filename, "r") as f:
                registered_save[filename] = [False, json.load(f)]
        except:
            registered_save[filename] = [False, {}]

        def g(*args, **kwargs):
            cache_key = str((args, tuple((k, a[k]) for k in sorted(kwargs.keys()))))
            if cache_key not in registered_save[filename][1]:
                registered_save[filename][1][cache_key] = fn(*args, **kwargs)
                registered_save[filename][0] = True

            return registered_save[filename][1][cache_key]

        return g

    return decorator


@file_backed_cache("data/member_ids.json")
def get_member_ids():
    print("[GET] Member IDs", file=sys.stderr)
    chambers = {
        "house": range(MIN_CONGRESS_NUM, MAX_CONGRESS_NUM + 1),
        "senate": range(MIN_CONGRESS_NUM, MAX_CONGRESS_NUM + 1),
    }
    member_ids = set()
    for (chamber, congress_range) in chambers.items():
        for congress in congress_range:
            response = requests.get(
                PROPUBLICA_API_URL_BASE
                + "/congress/v1/{}/{}/members.json".format(congress, chamber),
                headers=PROPUBLICA_API_HEADERS,
            ).json()
            for result in response["results"]:
                for member in result["members"]:
                    member_ids.add(member["id"])

    return list(member_ids)


@file_backed_cache("data/members.json")
def get_member(member_id):
    print("[GET] Member:", member_id, file=sys.stderr)
    response = requests.get(
        PROPUBLICA_API_URL_BASE + "/congress/v1/members/{}.json".format(member_id),
        headers=PROPUBLICA_API_HEADERS,
    ).json()
    return response["results"][0]


@file_backed_cache("data/committees.json")
def get_committee(congress, committee_code):
    print("[GET] Committee:", congress, committee_code, file=sys.stderr)
    if committee_code[0] == "H":
        chamber = "house"
    elif committee_code[0] == "S":
        chamber = "senate"
    else:
        chamber = "joint"
    response = requests.get(
        PROPUBLICA_API_URL_BASE
        + "/congress/v1/{}/{}/committees/{}.json".format(
            congress, chamber, committee_code
        ),
        headers=PROPUBLICA_API_HEADERS,
    ).json()
    return response["results"][0]


@file_backed_cache("data/subcommittees.json")
def get_subcommittee(congress, committee_code, subcommittee_code):
    print(
        "[GET] Subcommittee:",
        congress,
        committee_code,
        subcommittee_code,
        file=sys.stderr,
    )
    if committee_code[0] == "H":
        chamber = "house"
    elif committee_code[0] == "S":
        chamber = "senate"
    else:
        chamber = "joint"
    response = requests.get(
        PROPUBLICA_API_URL_BASE
        + "/congress/v1/{}/{}/committees/{}/subcommittees/{}.json".format(
            congress, chamber, committee_code, subcommittee_code
        ),
        headers=PROPUBLICA_API_HEADERS,
    ).json()
    try:
        return response["results"][0]
    except Exception as e:
        print(
            "error getting subcommittee {} for committee {}/{}".format(
                subcommittee_code, committee_code, congress
            ),
            file=sys.stderr,
        )
        return None


@file_backed_cache("data/committee_ids.json")
def get_committee_codes():
    print("[GET] Committee Codes", file=sys.stderr)
    committee_codes = set()
    for congress in range(MIN_CONGRESS_NUM, MAX_CONGRESS_NUM + 1):
        for chamber in ["house", "senate", "joint"]:
            response = requests.get(
                PROPUBLICA_API_URL_BASE
                + "/congress/v1/{}/{}/committees.json".format(congress, chamber),
                headers=PROPUBLICA_API_HEADERS,
            ).json()
            for result in response["results"]:
                for committee in result["committees"]:
                    committee_codes.add((result["congress"], committee["id"]))
    return list(committee_codes)


@file_backed_cache("data/propublica_specific_bill.json")
def get_specific_bill(bill_id):
    print("[GET] Specific bill:", bill_id, file=sys.stderr)
    split_id = bill_id.split("-")
    short_id = split_id[0]
    congress = int(split_id[1])
    assert congress >= MIN_CONGRESS_NUM and congress <= MAX_CONGRESS_NUM
    response = requests.get(
        PROPUBLICA_API_URL_BASE
        + "/congress/v1/{}/bills/{}.json".format(congress, short_id),
        headers=PROPUBLICA_API_HEADERS,
    ).json()
    return response["results"][0]


@file_backed_cache("data/bill_votes.json")
def get_rollcall(api_url):
    print("[GET] Vote from URI:", api_url, file=sys.stderr)
    response = requests.get(api_url, headers=PROPUBLICA_API_HEADERS).json()
    return response["results"]


@file_backed_cache("data/bioguide_html.json")
def get_bioguide_webpage(member_id):
    print("[GET] Bioguide webpage:", member_id, file=sys.stderr)
    response = requests.get(
        "http://bioguide.congress.gov/scripts/biodisplay.pl?index={}".format(member_id)
    )
    return response.text


@file_backed_cache("data/issue_lobbying.json")
def get_issue_lobbying(issue_name):
    subs = {
        "Metals": "Mining",
        "Nevada": "State",
        "Oregon": "State",
        "Sudan": "Africa",
        "Motor carriers": "Transportation",
        "Motor vehicles": "Transportation",
        "Motor fuels": "Transportation",
        "Hawaii": "State",
        "Self-employed": "Jobs",
        "Geography": "Science",
        "Materials": "Manufacturing",
        "Luxembourg": "Europe",
    }
    print("[GET] Issue Lobbying:", issue_name, file=sys.stderr)
    if issue_name in subs:
        issue_name = subs[issue_name]
    response = requests.get(
        "https://api.propublica.org/congress/v1/lobbying/search.json?query={}".format(
            issue_name
        ),
        headers=PROPUBLICA_API_HEADERS,
    ).json()
    result = None
    if (
        len(response["results"]) > 0
        and len(response["results"][0]["lobbying_representations"]) > 0
        and len(response["results"][0]["lobbying_representations"][0]["lobbyists"]) > 0
    ):
        result = response["results"][0]["lobbying_representations"][0]["lobbyists"][0][
            "name"
        ]
    return result


_id_counter = 0


def unique_id():
    global _id_counter
    _id_counter += 1
    return _id_counter


def make_sql_string(s):
    if s is None:
        return "NULL"
    else:
        return "'{}'".format(
            str(s).replace("'", "''").replace("\n", "\\n").replace("\r", "")
        )


def make_sql_date(d):
    return make_sql_string(d)


def make_sql_int(i):
    if i is None:
        return "NULL"
    else:
        return int(i)


def make_sql_float(i):
    if i is None:
        return "NULL"
    else:
        return float(i)


class SqlInsert:
    def __init__(self, statement, values):
        self.statement = statement
        self.values = values


class SqlBatchInsert:
    def __init__(self, inserts):
        MAX_BATCH_INSERT = 2000
        self.groups = []
        for i in inserts:
            if len(self.groups) == 0:
                self.groups.append((i.statement, [i.values]))
            else:
                if (
                    self.groups[-1][0] == i.statement
                    and len(self.groups[-1][1]) < MAX_BATCH_INSERT
                ):
                    self.groups[-1][1].append(i.values)
                else:
                    self.groups.append((i.statement, [i.values]))

    def export(self):
        lines = []
        for g in self.groups:
            lines.append("{} {}; show warnings;".format(g[0], ", ".join(g[1])))
        return "\n".join(lines)


def get_all_bills():
    PROPUBLICA_BILL_URL = (
        "https://s3.amazonaws.com/pp-projects-static/congress/bills/{}.zip"
    )
    BILL_BLACKLIST = [
        "hres856-115",
        "hres726-115",
        "hres705-115",
        "hres878-115",
        "hres646-115",
    ]
    bills = []
    for congress in range(MIN_CONGRESS_NUM, MAX_CONGRESS_NUM + 1):
        base = "data/congress/data/{}".format(congress)
        if not os.path.isdir(base):
            print("[GET] Bills for {}".format(congress), file=sys.stderr)
            zip_buffer = requests.get(PROPUBLICA_BILL_URL.format(congress)).content
            z = zipfile.ZipFile(io.BytesIO(zip_buffer))
            z.extractall("data")
        for root, dirs, files in os.walk(base):
            for name in files:
                if name.endswith(".json"):
                    file_path = os.path.join(root, name)
                    with open(file_path, "r") as fp:
                        raw_bill_data = json.load(fp)
                        bill_id = raw_bill_data["bill_id"]
                        if bill_id not in BILL_BLACKLIST:
                            specific_bill_data = get_specific_bill(
                                raw_bill_data["bill_id"]
                            )
                            bills.append(Bill(raw_bill_data, specific_bill_data))
    return bills


class Issue_Bill:

    id_map = {}

    @staticmethod
    def all_values():
        return (x for x in Issue_Bill.id_map.values() if x is not None)

    def __init__(self, issue, bill):
        self._issue = issue
        self._bill = bill
        Issue_Bill.id_map[(self._issue.issue_id(), self._bill.bill_id())] = self

    @staticmethod
    def sql_create_table():
        return """
        CREATE TABLE Issue_Bill (
          issue_id INT NOT NULL,
          bill_id VARCHAR(20) NOT NULL,
          PRIMARY KEY (issue_id, bill_id),
          FOREIGN KEY (issue_id) REFERENCES Issue(issue_id) ON DELETE CASCADE ON UPDATE CASCADE,
          FOREIGN KEY (bill_id) REFERENCES Bill(bill_id) ON DELETE CASCADE ON UPDATE CASCADE
        );
        """.strip()

    def sql_insert(self):
        return SqlInsert(
            "INSERT INTO Issue_Bill (issue_id, bill_id) VALUES ",
            "({issue_id}, {bill_id})".format(
                issue_id=self._issue.issue_id(),
                bill_id=make_sql_string(self._bill.bill_id()),
            ),
        )


class Issue_Member:

    id_map = {}

    @staticmethod
    def all_values():
        return (x for x in Issue_Member.id_map.values() if x is not None)

    def __init__(self, issue, member):
        self._issue = issue
        self._member = member
        Issue_Member.id_map[(self._issue.issue_id(), self._member.member_id())] = self

    @staticmethod
    def sql_create_table():
        return """
        CREATE TABLE Issue_Member (
          issue_id INT NOT NULL,
          member_id VARCHAR(20) NOT NULL,
          PRIMARY KEY (issue_id, member_id),
          FOREIGN KEY (issue_id) REFERENCES Issue(issue_id) ON DELETE CASCADE ON UPDATE CASCADE,
          FOREIGN KEY (member_id) REFERENCES Member(member_id) ON DELETE CASCADE ON UPDATE CASCADE
        );
        """.strip()

    def sql_insert(self):
        return SqlInsert(
            "INSERT INTO Issue_Member (issue_id, member_id) VALUES ",
            "({issue_id}, {member_id})".format(
                issue_id=self._issue.issue_id(),
                member_id=make_sql_string(self._member.member_id()),
            ),
        )


class Issue:

    id_map = {}

    @staticmethod
    def all_values():
        return (x for x in Issue.id_map.values() if x is not None)

    @staticmethod
    def from_name(issue_name):
        key = issue_name.strip().lower()
        if key not in Issue.id_map:
            Issue.id_map[key] = Issue(issue_name)
        return Issue.id_map[key]

    def __init__(self, issue_name):
        PICTURE_DIR = os.path.join(
            os.path.dirname(__file__), "../backend/static/SWEStockIssuePictures"
        )

        self._picture = None
        for s in os.listdir(PICTURE_DIR):
            if os.path.splitext(s)[0] == issue_name and os.path.isfile(
                os.path.join(PICTURE_DIR, s)
            ):
                self._picture = "/static/SWEStockIssuePictures/" + s

        self._name = issue_name
        self._id = unique_id()
        self._lobbying = get_issue_lobbying(self._name)

    def issue_id(self):
        return self._id

    def name(self):
        return self._name

    def picture(self):
        return self._picture

    def lobbying(self):
        return self._lobbying

    @staticmethod
    def sql_create_table():
        return """
        CREATE TABLE Issue (
          issue_id INT NOT NULL,
          name VARCHAR(255) NOT NULL UNIQUE,
          picture_url VARCHAR(255),
          lobbying VARCHAR(255),
          PRIMARY KEY (issue_id)
        );
        """.strip()

    def sql_insert(self):
        return SqlInsert(
            "INSERT INTO Issue (issue_id, name, picture_url, lobbying) VALUES ",
            "({issue_id}, {name}, {picture_url}, {lobbying})".format(
                issue_id=self.issue_id(),
                name=make_sql_string(self.name()),
                picture_url=make_sql_string(self.picture()),
                lobbying=make_sql_string(self.lobbying()),
            ),
        )


class RollCallPosition:

    id_map = {}

    @staticmethod
    def all_values():
        return (x for x in RollCallPosition.id_map.values() if x is not None)

    def __init__(self, roll_call, member, position_data):
        self.raw_data = position_data
        self._roll_call = roll_call
        self._member = member
        self._id = unique_id()
        RollCallPosition.id_map[
            (self._roll_call.rollcall_id(), self._member.member_id())
        ] = self

    def rollcallposition_id(self):
        return self._id

    def dw_nominate(self):
        return self.raw_data["dw_nominate"]

    def vote_position(self):
        return self.raw_data["vote_position"][0].upper()

    # note: not normalized because of sqlalchemy issues with association_proxy
    def roll_call_date(self):
        return self._roll_call.date()

    @staticmethod
    def sql_create_table():
        return """
        CREATE TABLE RollCallPosition (
          rollcallposition_id INT NOT NULL,
          rollcall_id INT NOT NULL,
          member_id CHAR(7) NOT NULL,
          vote_position CHAR(1) NOT NULL,
          dw_nominate FLOAT,
          roll_call_date DATE,
          PRIMARY KEY (rollcallposition_id),
          FOREIGN KEY (rollcall_id) REFERENCES RollCall(rollcall_id) ON DELETE CASCADE ON UPDATE CASCADE,
          FOREIGN KEY (member_id) REFERENCES Member(member_id) ON DELETE CASCADE ON UPDATE CASCADE
        );
        """.strip()

    def sql_insert(self):
        return SqlInsert(
            "INSERT INTO RollCallPosition (rollcallposition_id, rollcall_id, member_id, vote_position, dw_nominate, roll_call_date) VALUES ",
            "({rollcallposition_id}, {rollcall_id}, {member_id}, {vote_position}, {dw_nominate}, {roll_call_date})".format(
                rollcallposition_id=self.rollcallposition_id(),
                rollcall_id=self._roll_call.rollcall_id(),
                member_id=make_sql_string(self._member.member_id()),
                vote_position=make_sql_string(self.vote_position()),
                dw_nominate=make_sql_float(self.dw_nominate()),
                roll_call_date=make_sql_date(self.roll_call_date()),
            ),
        )


class RollCall:

    id_map = {}

    @staticmethod
    def all_values():
        return (x for x in RollCall.id_map.values() if x is not None)

    def __init__(self, bill, rollcall_data):
        self.raw_data = rollcall_data["votes"]["vote"]
        self._bill = bill
        self._id = unique_id()
        self._roll_call_positions = [
            RollCallPosition(self, Member.from_id(p["member_id"]), p)
            for p in self.raw_data["positions"]
        ]
        RollCall.id_map[self._id] = self

    def rollcall_id(self):
        return self._id

    def date(self):
        return self.raw_data["date"]

    def congress(self):
        return int(self.raw_data["congress"])

    def session(self):
        return self.raw_data["session"]

    def chamber(self):
        return self.raw_data["chamber"][0].upper()

    def roll_call_num(self):
        return self.raw_data["roll_call"]

    @staticmethod
    def sql_create_table():
        return """
        CREATE TABLE RollCall (
          rollcall_id INT NOT NULL PRIMARY KEY,
          bill_id VARCHAR(20) NOT NULL,
          roll_call_date DATE NOT NULL,
          session_number INT NOT NULL,
          chamber CHAR(1) NOT NULL,
          roll_call_number INT NOT NULL,
          congress INT NOT NULL,
          FOREIGN KEY (bill_id) REFERENCES Bill(bill_id) ON DELETE CASCADE ON UPDATE CASCADE
        );
        """.strip()

    def sql_insert(self):
        return SqlInsert(
            "INSERT INTO RollCall (rollcall_id, bill_id, roll_call_date, session_number, chamber, roll_call_number, congress) VALUES ",
            "({rollcall_id}, {bill_id}, {roll_call_date}, {session_number}, {chamber}, {roll_call_number}, {congress})".format(
                rollcall_id=self.rollcall_id(),
                bill_id=make_sql_string(self._bill.bill_id()),
                roll_call_date=make_sql_date(self.date()),
                session_number=self.session(),
                chamber=make_sql_string(self.chamber()),
                roll_call_number=self.roll_call_num(),
                congress=self.congress(),
            ),
        )


class BillCosponsor:

    id_map = {}

    @staticmethod
    def all_values():
        return (x for x in BillCosponsor.id_map.values() if x is not None)

    def __init__(self, bill, cosponsor_member):
        self._member = cosponsor_member
        self._bill = bill
        BillCosponsor.id_map[(self._bill.bill_id(), self._member.member_id())] = self

    @staticmethod
    def sql_create_table():
        return """
        CREATE TABLE Bill_Cosponsor (
          bill_id VARCHAR(20) NOT NULL,
          cosponsor_id CHAR(7) NOT NULL,
          PRIMARY KEY (bill_id, cosponsor_id),
          FOREIGN KEY (bill_id) REFERENCES Bill(bill_id) ON DELETE CASCADE ON UPDATE CASCADE,
          FOREIGN KEY (cosponsor_id) REFERENCES Member(member_id) ON DELETE CASCADE ON UPDATE CASCADE
        );
        """.strip()

    def sql_insert(self):
        return SqlInsert(
            "INSERT INTO Bill_Cosponsor (bill_id, cosponsor_id) VALUES ",
            "({bill_id}, {cosponsor_id})".format(
                bill_id=make_sql_string(self._bill.bill_id()),
                cosponsor_id=make_sql_string(self._member.member_id()),
            ),
        )


class BillCommittee:

    id_map = {}

    @staticmethod
    def all_values():
        return (x for x in BillCommittee.id_map.values() if x is not None)

    def __init__(self, bill, committee):
        self._bill = bill
        self._committee = committee
        BillCommittee.id_map[
            (self._bill.bill_id, self._committee.committee_id())
        ] = self

    @staticmethod
    def sql_create_table():
        return """
        CREATE TABLE Bill_Committee (
          bill_id VARCHAR(20) NOT NULL,
          committee_id INT NOT NULL,
          PRIMARY KEY (bill_id, committee_id),
          FOREIGN KEY (bill_id) REFERENCES Bill(bill_id) ON DELETE CASCADE ON UPDATE CASCADE,
          FOREIGN KEY (committee_id) REFERENCES Committee(committee_id) ON DELETE CASCADE ON UPDATE CASCADE

        );
        """.strip()

    def sql_insert(self):
        return SqlInsert(
            "INSERT INTO Bill_Committee (bill_id, committee_id) VALUES ",
            "({bill_id}, {committee_id})".format(
                bill_id=make_sql_string(self._bill.bill_id()),
                committee_id=self._committee.committee_id(),
            ),
        )


class BillSubcommittee:

    id_map = {}

    @staticmethod
    def all_values():
        return (x for x in BillSubcommittee.id_map.values() if x is not None)

    def __init__(self, bill, subcommittee):
        self._bill = bill
        self._subcommittee = subcommittee
        BillSubcommittee.id_map[
            (self._bill.bill_id, self._subcommittee.subcommittee_id())
        ] = self

    @staticmethod
    def sql_create_table():
        return """
        CREATE TABLE Bill_Subcommittee (
          bill_id VARCHAR(20) NOT NULL,
          subcommittee_id INT NOT NULL,
          PRIMARY KEY (bill_id, subcommittee_id),
          FOREIGN KEY (bill_id) REFERENCES Bill(bill_id) ON DELETE CASCADE ON UPDATE CASCADE,
          FOREIGN KEY (subcommittee_id) REFERENCES Subcommittee(subcommittee_id) ON DELETE CASCADE ON UPDATE CASCADE
        );
        """.strip()

    def sql_insert(self):
        return SqlInsert(
            "INSERT INTO Bill_Subcommittee (bill_id, subcommittee_id) VALUES ",
            "({bill_id}, {subcommittee_id})".format(
                bill_id=make_sql_string(self._bill.bill_id()),
                subcommittee_id=self._subcommittee.subcommittee_id(),
            ),
        )


class Bill:

    id_map = {}

    @staticmethod
    def all_values():
        return (x for x in Bill.id_map.values() if x is not None)

    def __init__(self, bill_data, specific_bill_data):
        self.raw_data = bill_data
        congress = int(self.raw_data["congress"])
        self.specific_bill_data = specific_bill_data
        if MIN_CONGRESS_NUM <= congress <= MAX_CONGRESS_NUM:
            self._cosponsors = [
                BillCosponsor(self, Member.from_id(c))
                for c in set(x["bioguide_id"] for x in self.raw_data["cosponsors"])
            ]
            self._committees = [
                BillCommittee(self, Committee.from_id(congress, code))
                for code in self.specific_bill_data["committee_codes"]
            ]
            self._subcommittees = []
            for subcommittee_code in self.specific_bill_data["subcommittee_codes"]:
                sc = Subcommittee.from_id(
                    congress,
                    Committee.from_id(congress, subcommittee_code[:4]),
                    subcommittee_code,
                )
                if sc is None:
                    continue
                self._subcommittees.append(BillSubcommittee(self, sc))
        else:
            self._cosponsors = []
            self._committees = []
            self._subcommittees = []

        self._rollcalls = [
            RollCall(self, get_rollcall(vote["api_url"]))
            for vote in self.specific_bill_data["votes"]
        ]
        if (
            "sponsor_id" in self.specific_bill_data
            and self.specific_bill_data["sponsor_id"] is not None
        ):
            self._sponsoring_member = Member.from_id(
                self.specific_bill_data["sponsor_id"]
            )
        else:
            self._sponsoring_member = None

        if "subjects" in self.raw_data and self.raw_data["subjects"] is not None:
            self._issues = [
                Issue_Bill(Issue.from_name(issue_name), self)
                for issue_name in self.raw_data["subjects"]
            ]
            if self._sponsoring_member is not None:
                self._issues_members = [
                    Issue_Member(Issue.from_name(issue_name), self._sponsoring_member)
                    for issue_name in self.raw_data["subjects"]
                ]

        Bill.id_map[self.bill_id()] = self

    def bill_id(self):
        return self.specific_bill_data["bill_id"]

    def congress(self):
        return int(self.raw_data["congress"])

    def sponsoring_member_id(self):
        if self._sponsoring_member is None:
            return None
        else:
            return self._sponsoring_member.member_id()

    def short_title(self):
        return self.specific_bill_data["short_title"]

    def full_title(self):
        return self.specific_bill_data["title"]

    def summary(self):
        return self.specific_bill_data["summary"]

    def house_passage_date(self):
        return self.specific_bill_data["house_passage"]

    def senate_passage_date(self):
        return self.specific_bill_data["senate_passage"]

    def enacted_date(self):
        return self.specific_bill_data["enacted"]

    def vetoed_date(self):
        return self.specific_bill_data["vetoed"]

    @staticmethod
    def sql_create_table():
        return """
        CREATE TABLE Bill (
          bill_id VARCHAR(20) NOT NULL PRIMARY KEY,
          sponsoring_member_id CHAR(7),
          short_title VARCHAR(255),
          full_title LONGTEXT,
          summary LONGTEXT,
          house_passage_date DATE,
          senate_passage_date DATE,
          enacted_date DATE,
          vetoed_date DATE,
          congress INT NOT NULL,
          FOREIGN KEY (sponsoring_member_id) REFERENCES Member(member_id) ON DELETE CASCADE ON UPDATE CASCADE
        );
        """.strip()

    def sql_insert(self):
        return SqlInsert(
            "INSERT INTO Bill (bill_id, sponsoring_member_id, short_title, full_title, summary, house_passage_date, senate_passage_date, enacted_date, vetoed_date, congress) VALUES ",
            "({bill_id}, {sponsoring_member_id}, {short_title}, {full_title}, {summary}, {house_passage_date}, {senate_passage_date}, {enacted_date}, {vetoed_date}, {congress})".format(
                bill_id=make_sql_string(self.bill_id()),
                sponsoring_member_id=make_sql_string(self.sponsoring_member_id()),
                short_title=make_sql_string(self.short_title()),
                full_title=make_sql_string(self.full_title()),
                summary=make_sql_string(self.summary()),
                house_passage_date=make_sql_date(self.house_passage_date()),
                senate_passage_date=make_sql_date(self.senate_passage_date()),
                enacted_date=make_sql_date(self.enacted_date()),
                vetoed_date=make_sql_date(self.vetoed_date()),
                congress=make_sql_int(self.congress()),
            ),
        )


class Subcommittee:
    id_map = {}

    @staticmethod
    def from_id(congress, committee, subcommittee_code):
        key = (congress, committee, subcommittee_code)
        if key not in Subcommittee.id_map:
            data = get_subcommittee(
                congress, committee.committee_code(), subcommittee_code
            )
            if data is not None:
                Subcommittee.id_map[key] = Subcommittee(congress, committee, data)
            else:
                Subcommittee.id_map[key] = None
        return Subcommittee.id_map[key]

    @staticmethod
    def all_values():
        return (x for x in Subcommittee.id_map.values() if x is not None)

    def __init__(self, congress, parent_committee, subcommittee_data):
        self.raw_data = subcommittee_data
        self._id = unique_id()
        self._parent_committee = parent_committee
        self._congress = congress

    def subcommittee_id(self):
        return self._id

    def subcommittee_code(self):
        return self.raw_data["id"]

    def parent_committee_id(self):
        return self._parent_committee.committee_id()

    def congress(self):
        return int(self._congress)

    def name(self):
        return self.raw_data["name"]

    def website(self):
        if "url" in self.raw_data:
            return self.raw_data["url"]
        else:
            return None

    def chair_party(self):
        if (
            "chair_party" in self.raw_data
            and self.raw_data["chair_party"] is not None
            and len(self.raw_data["chair_party"]) > 0
        ):
            return self.raw_data["chair_party"][0].upper()
        else:
            return None

    @staticmethod
    def sql_create_table():
        return """
        CREATE TABLE Subcommittee (
          subcommittee_id INT NOT NULL,
          subcommittee_code CHAR(6) NOT NULL,
          parent_committee_id INT NOT NULL,
          name VARCHAR(255) NOT NULL,
          website VARCHAR(255),
          chair_party CHAR(1),
          PRIMARY KEY (subcommittee_id),
          FOREIGN KEY (parent_committee_id) REFERENCES Committee(committee_id) ON DELETE CASCADE ON UPDATE CASCADE
        );
        """.strip()

    def sql_insert(self):
        return SqlInsert(
            "INSERT INTO Subcommittee (subcommittee_id, subcommittee_code, parent_committee_id, name, website, chair_party) VALUES ",
            "({subcommittee_id}, {subcommittee_code}, {parent_committee_id}, {name}, {website}, {chair_party})".format(
                subcommittee_id=self.subcommittee_id(),
                subcommittee_code=make_sql_string(self.subcommittee_code()),
                parent_committee_id=self.parent_committee_id(),
                name=make_sql_string(self.name()),
                website=make_sql_string(self.website()),
                chair_party=make_sql_string(self.chair_party()),
            ),
        )


class Committee:
    id_map = {}

    @staticmethod
    def from_id(congress, committee_code):
        if committee_code == "HLIG":
            committee_code = "HSIG"
        key = (congress, committee_code)

        if key not in Committee.id_map:
            data = get_committee(*key)
            Committee.id_map[key] = Committee(congress, data)
        committee = Committee.id_map[key]
        assert (
            committee.committee_code() == committee_code
            and committee.congress() == congress
        )
        return committee

    @staticmethod
    def all_values():
        return (x for x in Committee.id_map.values() if x is not None)

    def __init__(self, congress, data):
        self.raw_data = data
        self._id = unique_id()
        self._congress = congress
        self._subcommittees = []
        for subcommittee_code in map(lambda x: x["id"], self.raw_data["subcommittees"]):
            sc = Subcommittee.from_id(congress, self, subcommittee_code)
            if sc is not None:
                self._subcommittees.append(sc)

    def committee_id(self):
        return self._id

    def committee_code(self):
        return self.raw_data["id"]

    def congress(self):
        return int(self._congress)

    def name(self):
        return self.raw_data["name"]

    def website(self):
        return self.raw_data["url"]

    def chair_party(self):
        if (
            "chair_party" in self.raw_data
            and self.raw_data["chair_party"] is not None
            and len(self.raw_data["chair_party"]) > 0
        ):
            return self.raw_data["chair_party"][0].upper()
        else:
            return None

    @staticmethod
    def sql_create_table():
        return """
        CREATE TABLE Committee (
          committee_id INT NOT NULL,
          committee_code CHAR(4) NOT NULL,
          congress INT NOT NULL,
          name VARCHAR(255) NOT NULL,
          website VARCHAR(255),
          chair_party CHAR(1),
          PRIMARY KEY (committee_id)
        );
        """.strip()

    def sql_insert(self):
        return SqlInsert(
            "INSERT INTO Committee (committee_id, committee_code, congress, name, website, chair_party) VALUES ",
            "({committee_id}, {committee_code}, {congress}, {name}, {website}, {chair_party})".format(
                committee_id=self.committee_id(),
                committee_code=make_sql_string(self.committee_code()),
                congress=self.congress(),
                name=make_sql_string(self.name()),
                website=make_sql_string(self.website()),
                chair_party=make_sql_string(self.chair_party()),
            ),
        )


class MemberRoleSubcommittee:

    id_map = {}

    @staticmethod
    def all_values():
        return (x for x in MemberRoleSubcommittee.id_map.values() if x is not None)

    def __init__(self, role, subcommittee, data):
        self.raw_data = data
        self._id = unique_id()
        self._role = role
        self._subcommittee = subcommittee
        assert self._subcommittee is not None
        MemberRoleSubcommittee.id_map[self._id] = self

    def role_subcommittee_id(self):
        return self._id

    def member_role_id(self):
        return self._role.role_id()

    def subcommittee_id(self):
        return self._subcommittee.subcommittee_id()

    def title(self):
        return self.raw_data["title"]

    def rank_in_party(self):
        rank = self.raw_data["rank_in_party"]
        if rank is None:
            return None
        else:
            return int(rank)

    @staticmethod
    def sql_create_table():
        return """
        CREATE TABLE Member_Role_Subcommittee (
          member_role_subcommittee_id INT NOT NULL PRIMARY KEY,
          member_role_id INT NOT NULL,
          subcommittee_id INT NOT NULL,
          title VARCHAR(255) NOT NULL,
          rank_in_party INT,
          FOREIGN KEY (member_role_id) REFERENCES Member_Role(member_role_id) ON DELETE CASCADE ON UPDATE CASCADE,
          FOREIGN KEY (subcommittee_id) REFERENCES Subcommittee(subcommittee_id) ON DELETE CASCADE ON UPDATE CASCADE
        );
        """.strip()

    def sql_insert(self):
        return SqlInsert(
            "INSERT INTO Member_Role_Subcommittee (member_role_subcommittee_id, member_role_id, subcommittee_id, title, rank_in_party) VALUES ",
            "({member_role_subcommittee_id}, {member_role_id}, {subcommittee_id}, {title}, {rank_in_party})".format(
                member_role_subcommittee_id=self.role_subcommittee_id(),
                member_role_id=self.member_role_id(),
                subcommittee_id=self.subcommittee_id(),
                title=make_sql_string(self.title()),
                rank_in_party=make_sql_int(self.rank_in_party()),
            ),
        )


class MemberRoleCommittee:

    id_map = {}

    @staticmethod
    def all_values():
        return (x for x in MemberRoleCommittee.id_map.values() if x is not None)

    def __init__(self, role, data):
        self.raw_data = data
        self._id = unique_id()
        self._role = role
        self._committee = Committee.from_id(self._role.congress(), data["code"])
        MemberRoleCommittee.id_map[self._id] = self

    def role_committee_id(self):
        return self._id

    def member_role_id(self):
        return self._role.role_id()

    def committee_id(self):
        return self._committee.committee_id()

    def title(self):
        return self.raw_data["title"]

    def rank_in_party(self):
        rank = self.raw_data["rank_in_party"]
        if rank is None:
            return None
        else:
            return int(rank)

    @staticmethod
    def sql_create_table():
        return """
        CREATE TABLE Member_Role_Committee (
          member_role_committee_id INT NOT NULL PRIMARY KEY,
          member_role_id INT NOT NULL,
          committee_id INT NOT NULL,
          title VARCHAR(255) NOT NULL,
          rank_in_party INT,
          FOREIGN KEY (member_role_id) REFERENCES Member_Role(member_role_id) ON DELETE CASCADE ON UPDATE CASCADE,
          FOREIGN KEY (committee_id) REFERENCES Committee(committee_id) ON DELETE CASCADE ON UPDATE CASCADE
        );
        """.strip()

    def sql_insert(self):
        return SqlInsert(
            "INSERT INTO Member_Role_Committee (member_role_committee_id, member_role_id, committee_id, title, rank_in_party) VALUES ",
            "({member_role_committee_id}, {member_role_id}, {committee_id}, {title}, {rank_in_party})"
            "".format(
                member_role_committee_id=self.role_committee_id(),
                member_role_id=self.member_role_id(),
                committee_id=self.committee_id(),
                title=make_sql_string(self.title()),
                rank_in_party=make_sql_int(self.rank_in_party()),
            ),
        )


class MemberRole:

    id_map = {}

    @staticmethod
    def all_values():
        return (x for x in MemberRole.id_map.values() if x is not None)

    def __init__(self, member, data):
        self.raw_data = data
        self._member = member
        self._id = unique_id()
        self._committees = [
            MemberRoleCommittee(self, committee_data)
            for committee_data in self.raw_data["committees"]
        ]

        self._subcommittees = []
        for subcommittee_data in self.raw_data["subcommittees"]:
            sc = Subcommittee.from_id(
                self.congress(),
                Committee.from_id(
                    self.congress(), subcommittee_data["parent_committee_id"]
                ),
                subcommittee_data["code"],
            )
            if sc is not None:
                self._subcommittees.append(
                    MemberRoleSubcommittee(self, sc, subcommittee_data)
                )

        MemberRole.id_map[self._id] = self

    def role_id(self):
        return self._id

    def member_id(self):
        return self._member.member_id()

    def congress(self):
        return int(self.raw_data["congress"])

    def chamber(self):
        return self.raw_data["chamber"][0].upper()

    def title(self):
        return self.raw_data["title"]

    def short_title(self):
        return self.raw_data["short_title"]

    def state(self):
        return self.raw_data["state"]

    def party(self):
        return self.raw_data["party"][0].upper()

    def leadership_role(self):
        if "leadership_role" in self.raw_data:
            return self.raw_data["leadership_role"]
        else:
            return None

    def district(self):
        if "district" in self.raw_data:
            d = self.raw_data["district"]
            if d == "At-Large":
                return 0
            else:
                return int(d)
        else:
            return None

    def missed_votes_percent(self):
        missed_votes_percent_str = None
        if "missed_votes_pct" in self.raw_data:
            missed_votes_percent_str = self.raw_data["missed_votes_pct"]

        if missed_votes_percent_str is not None:
            return float(missed_votes_percent_str)
        else:
            return None

    def votes_with_party_percent(self):
        votes_with_party_percent_str = None
        if "votes_with_party_pct" in self.raw_data:
            votes_with_party_percent_str = self.raw_data["votes_with_party_pct"]

        if votes_with_party_percent_str is not None:
            return float(votes_with_party_percent_str)
        else:
            return None

    def start_date(self):
        return self.raw_data["start_date"]

    def end_date(self):
        return self.raw_data["end_date"]

    def committees(self):
        return self._committees

    def subcommittees(self):
        return self._subcommittees

    @staticmethod
    def sql_create_table():
        # member_role_id autoincrement
        return """
        CREATE TABLE Member_Role (
          member_role_id INT NOT NULL PRIMARY KEY,
          member_id CHAR(7) NOT NULL,
          congress INT NOT NULL,
          chamber CHAR(1) NOT NULL,
          title VARCHAR(255) NOT NULL,
          short_title VARCHAR(255),
          party CHAR(1) NOT NULL,
          leadership_role VARCHAR(255),
          district INT,
          state CHAR(2) NOT NULL,
          missed_votes_percent FLOAT,
          votes_with_party_percent FLOAT,
          start_date DATE NOT NULL,
          end_date DATE NOT NULL,
          FOREIGN KEY (member_id) REFERENCES Member(member_id) ON DELETE CASCADE ON UPDATE CASCADE
        );
        """.strip()

    def sql_insert(self):
        return SqlInsert(
            "INSERT INTO Member_Role (member_role_id, member_id, congress, chamber, title, short_title, party, leadership_role, district, state, missed_votes_percent, votes_with_party_percent, start_date, end_date) VALUES ",
            "({member_role_id}, {member_id}, {congress}, {chamber}, {title}, {short_title}, {party}, {leadership_role}, {district}, {state}, {missed_votes_percent}, {votes_with_party_percent}, {start_date}, {end_date})".format(
                member_role_id=self.role_id(),
                member_id=make_sql_string(self.member_id()),
                congress=self.congress(),
                chamber=make_sql_string(self.chamber()),
                title=make_sql_string(self.title()),
                short_title=make_sql_string(self.short_title()),
                party=make_sql_string(self.party()),
                leadership_role=make_sql_string(self.leadership_role()),
                district=make_sql_int(self.district()),
                state=make_sql_string(self.state()),
                missed_votes_percent=make_sql_float(self.missed_votes_percent()),
                votes_with_party_percent=make_sql_float(
                    self.votes_with_party_percent()
                ),
                start_date=make_sql_date(self.start_date()),
                end_date=make_sql_date(self.end_date()),
            ),
        )


class Member:

    id_map = {}

    @staticmethod
    def from_id(member_id):
        if member_id not in Member.id_map:
            member_data = get_member(member_id)
            Member.id_map[member_id] = Member(member_data)
        return Member.id_map[member_id]

    @staticmethod
    def all_values():
        return (x for x in Member.id_map.values() if x is not None)

    def __init__(self, member_data):
        PICTURE_DIR = os.path.join(
            os.path.dirname(__file__), "../backend/static/MemberPictures"
        )

        self.raw_data = member_data
        self.cosponsored = set()
        self._role_data = [
            MemberRole(self, r)
            for r in self.raw_data["roles"]
            if MIN_CONGRESS_NUM <= int(r["congress"]) <= MAX_CONGRESS_NUM
        ]

        bioguide_html = get_bioguide_webpage(self.member_id())
        parser = BeautifulSoup(bioguide_html, "html.parser")
        self._biography = parser.body.select("p")[0].text.strip()

        self._picture = None
        for s in os.listdir(PICTURE_DIR):
            if os.path.splitext(s)[0] == self.member_id() and os.path.isfile(
                os.path.join(PICTURE_DIR, s)
            ):
                self._picture = "/static/MemberPictures/" + s

    def member_id(self):
        return self.raw_data["member_id"]

    def biography(self):
        return self._biography

    def first_name(self):
        return self.raw_data["first_name"]

    def middle_name(self):
        if (
            self.raw_data["middle_name"] is None
            or len(self.raw_data["middle_name"].strip()) == 0
        ):
            return None
        else:
            return self.raw_data["middle_name"]

    def last_name(self):
        return self.raw_data["last_name"]

    def date_of_birth(self):
        return self.raw_data["date_of_birth"]

    def gender(self):
        return self.raw_data["gender"]

    def website(self):
        return self.raw_data["url"]

    def twitter_account(self):
        return self.raw_data["twitter_account"]

    def facebook_account(self):
        return self.raw_data["facebook_account"]

    def picture(self):
        return self._picture

    def party(self):
        if (
            "current_party" in self.raw_data
            and self.raw_data["current_party"] is not None
            and len(self.raw_data["current_party"]) > 0
        ):
            return self.raw_data["current_party"][0].upper()
        else:
            return None

    def date_of_birth(self):
        w = self.raw_data["date_of_birth"]
        assert w is not None
        return w

    def website(self):
        return self.raw_data["url"]

    @staticmethod
    def sql_create_table():
        return """
        CREATE TABLE Member (
          member_id CHAR(7) NOT NULL PRIMARY KEY,
          first_name VARCHAR(255) NOT NULL,
          middle_name VARCHAR(255),
          last_name VARCHAR(255) NOT NULL,
          twitter_account VARCHAR(255),
          facebook_account VARCHAR(255),
          party CHAR(1),
          biography LONGTEXT,
          date_of_birth DATE NOT NULL,
          website VARCHAR(255),
          picture_url VARCHAR(255)
        );
        """.strip()

    def sql_insert(self):
        return SqlInsert(
            "INSERT INTO Member (member_id, first_name, middle_name, last_name, twitter_account, facebook_account, party, biography, date_of_birth, website, picture_url) VALUES ",
            "({member_id}, {first_name}, {middle_name}, {last_name}, {twitter_account}, {facebook_account}, {party}, {biography}, {date_of_birth}, {website}, {picture_url})".format(
                member_id=make_sql_string(self.member_id()),
                first_name=make_sql_string(self.first_name()),
                middle_name=make_sql_string(self.middle_name()),
                last_name=make_sql_string(self.last_name()),
                twitter_account=make_sql_string(self.twitter_account()),
                facebook_account=make_sql_string(self.facebook_account()),
                party=make_sql_string(self.party()),
                biography=make_sql_string(self.biography()),
                date_of_birth=make_sql_date(self.date_of_birth()),
                website=make_sql_string(self.website()),
                picture_url=make_sql_string(self.picture()),
            ),
        )


def main():
    members = []
    committees = []
    for (congress, committee_code) in get_committee_codes():
        committees.append(Committee.from_id(int(congress), committee_code))

    for member_id in get_member_ids():
        members.append(Member.from_id(member_id))
    bills = get_all_bills()

    tables = [
        Member,
        Bill,
        RollCall,
        Committee,
        MemberRole,
        Subcommittee,
        BillCosponsor,
        BillCommittee,
        RollCallPosition,
        BillSubcommittee,
        MemberRoleCommittee,
        MemberRoleSubcommittee,
        Issue,
        Issue_Bill,
        Issue_Member,
    ]

    for table in tables:
        print(table.sql_create_table())

    for table in tables:
        print(SqlBatchInsert([x.sql_insert() for x in table.all_values()]).export())

    print(
        "CREATE VIEW IssueNumBills AS SELECT Issue.*, COUNT(CASE WHEN Bill.enacted_date IS NOT NULL THEN 1 ELSE NULL END) AS num_bills_passed, COUNT(Bill.bill_id) AS num_bills from Issue LEFT JOIN Issue_Bill using (issue_id) LEFT JOIN Bill using (bill_id) GROUP BY Issue.issue_id;"
    )


if __name__ == "__main__":
    main()
